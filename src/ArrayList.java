
public class ArrayList {
	
	
	int[] array;
	int capacity = 32;
	int currentIndex;
	
	ArrayList()
	{
		this.array = new int[32];
		this.currentIndex =0;
	}
	ArrayList(int Capacity)
	{
		this.array = new int[Capacity];
		this.currentIndex =0;
		this.capacity = Capacity;
	}
	
	public boolean add(int addedValue)
	{
		try {
		
		if(currentIndex >= capacity)
		{	
			this.capacity*=2;
			System.out.println(this.capacity);
			int[] newarray = new int[this.capacity];
			System.arraycopy(this.array, 0, newarray,  
	                 0, (this.array.length));
			this.array = newarray;
		
		}	
		
		this.array[this.currentIndex] = addedValue;	
		this.currentIndex ++;
	
		return true;	
		}
		catch(Exception e)
		{
			System.out.println("Error: "+e);
			return false;
		}
	
	}
	public int getSize()
	{
		return this.currentIndex;
	}
	
	public int getCapacity()
	{
		return this.capacity;
	}
	
	public int getElementatIndex(int Index)
	{
		return this.array[Index];
	}
}
